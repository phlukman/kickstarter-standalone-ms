#********************************************************************************************
# Collection of
#
#
#********************************************************************************************

AZURE_ECR=devsecopsecr.azurecr.io
AZ_PASSWORD=QuePinga!71
AZ_USER=patria.lukman@hybridcloud.bell.ca
AZ_SUBSCRIPTION=1aa978fe-a193-4745-8c2c-136abbf39e94
AZ_RESOURCE_GROUP=bellmedia-devsecops
AKS_NAME=devsecops-test

install_helm_client() {
  curl -fsSL -o get_helm.sh https://raw.githubusercontent.com/helm/helm/master/scripts/get-helm-3
  chmod 700 get_helm.sh
  ./get_helm.sh
  helm version
}

package_chart() {
  helm package .
  helm install kickstarter-standalone-ms kickstarter-standalone-ms-0.1.0.tgz --debug
}

push_chart_to_azure_remote_repo() {
  export HELM_EXPERIMENTAL_OCI=1
  export password=73GWEI=U3/vEvG5dYpBd7SnX5YMhEhuf
  echo $password | helm registry login devsecopsecr.azurecr.io --username devsecopsecr --password-stdin
  helm chart save . kickstarter-standalone-ms:v1
  helm chart save . ${AZURE_ECR}/helm/kickstarter-standalone-ms:v1
  helm chart list
  helm chart push ${AZURE_ECR}/helm/kickstarter-standalone-ms:v1
}

remove_chart_from_remote_repo() {
  az acr repository delete -y --name ${AZURE_ECR} --image helm/kickstarter-standalone-ms:v1
}
install_az_client() {
  brew update && brew install azure-cli
  az version
}

azure_k8s_login() {
  az login -u ${AZ_USER} -p ${AZ_PASSWORD}
  az account set --subscription ${AZ_SUBSCRIPTION}
  az aks get-credentials --resource-group ${AZ_RESOURCE_GROUP} --name ${AKS_NAME}
}

# main starts here
push_chart_to_remote_repo
